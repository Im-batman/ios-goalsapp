//
//  UIViewExt.swift
//  GoalPost
//
//  Created by Asar Sunny on 23/11/2018.
//  Copyright © 2018 Gurudeveloperinc. All rights reserved.
//

import UIKit

extension UIView{
    
    func bindToKeyBoard(){
        NotificationCenter.default.addObserver(self, selector: #selector(keyBoardWillChange(_:)), name: Notification.Name.UIKeyboardWillChangeFrame, object: nil)
        
    }
    
    
   @objc func keyBoardWillChange(_ notification:Notification){
        
    let duration = notification.userInfo![UIKeyboardAnimationDurationUserInfoKey] as! Double
    let curve = notification.userInfo![UIKeyboardAnimationCurveUserInfoKey] as! UInt
    let startingFarame = (notification.userInfo![UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
    let endFrame = (notification.userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
    
    //keyboard height
    let deltaY = endFrame.origin.y - startingFarame.origin.y

    UIView.animateKeyframes(withDuration: duration, delay: 0.0, options: UIViewKeyframeAnimationOptions.init(rawValue: curve), animations: {
        self.frame.origin.y += deltaY
    }, completion: nil)
    
    }
    
    
    
    
    
    
    
}
