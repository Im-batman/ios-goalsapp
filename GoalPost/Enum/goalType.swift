//
//  goalType.swift
//  GoalPost
//
//  Created by Asar Sunny on 23/11/2018.
//  Copyright © 2018 Gurudeveloperinc. All rights reserved.
//

import Foundation


enum goalType: String{
    case longTerm = "Long Term"
    case shortTerm = "Short Term"
}
