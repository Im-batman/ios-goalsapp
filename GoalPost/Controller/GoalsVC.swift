//
//  GoalsVC.swift
//  GoalPost
//
//  Created by Asar Sunny on 22/11/2018.
//  Copyright © 2018 Gurudeveloperinc. All rights reserved.
//

import UIKit
import CoreData


let appDelegate = UIApplication.shared.delegate as? AppDelegate

class GoalsVC: UIViewController {
    var goals: [Goal] = []

    //outlets
    @IBOutlet weak var tableView: UITableView!

    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchCoreData()
        
        tableView.reloadData()
    }
    
    
    
    func  fetchCoreData(){
   
        fetchGoals { (completed) in
            if completed{
                
                if goals.count > 0 {
                    tableView.isHidden = false
                    
                }else{
                    tableView.isHidden = true
                }
                
            }
        }
   
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.isHidden = false

        // Do any additional setup after loading the view.
    }

    @IBAction func addGoalBtnPress(_ sender: UIButton) {
        guard let createVC = storyboard?.instantiateViewController(withIdentifier: "CreateGoalVC") else {return}
        //custom present vc
        presentDetail(createVC)
        
        
        
        print("pressed!")
    }
    
   

}

// extends the main class
extension GoalsVC: UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return goals.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "goalCell", for: indexPath) as? GoalCell else {return UITableViewCell()}
         let goal = goals[indexPath.row]
        cell.configureCell(goal: goal)
        return cell
    }
    
    
    
    //methods for edit and delete
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return UITableViewCellEditingStyle.none
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let deleteAction = UITableViewRowAction(style: .destructive, title: "DELETE") { (rowAction, indexPath) in
            self.deleteGoal(atIndexPath: indexPath)
            self.fetchCoreData() 
            tableView.deleteRows(at: [indexPath], with: .automatic)
        }
        
        
        let addProgressAction = UITableViewRowAction(style: .normal, title: "Add 1") { (rowAction, indexPath) in
            self.addProgress(atIndexpath: indexPath)
            tableView.reloadRows(at: [indexPath], with: .automatic)
        }
        
        
        
        deleteAction.backgroundColor = #colorLiteral(red: 0.3176470697, green: 0.07450980693, blue: 0.02745098062, alpha: 1)
        addProgressAction.backgroundColor = #colorLiteral(red: 0.7254902124, green: 0.4784313738, blue: 0.09803921729, alpha: 1)
        return [deleteAction,addProgressAction]
        
    }
    
    
    
    
    
    
   
}






extension GoalsVC{
    
    //fetch goals
    func fetchGoals(completion: (_ completed:Bool)->()){
        
        guard let managedContext = appDelegate?.persistentContainer.viewContext else {return}
        let fetchRequest = NSFetchRequest<Goal>(entityName: "Goal")
        
        do {
          goals =  try managedContext.fetch(fetchRequest)
            completion(true)
        } catch  {
            debugPrint("could'nt fetch \(error.localizedDescription)")
        completion(false)
        }
        
        
    }
    
    
    //delete Goal
    
    func deleteGoal(atIndexPath indexPath:IndexPath){
        guard let managedContext = appDelegate?.persistentContainer.viewContext else {return}
        managedContext.delete(goals[indexPath.row])
        
        do {
            try managedContext.save()
            print("oh deleted")
        } catch  {
            debugPrint("could not delet \(error.localizedDescription)")
        }
        
    }
    
    
    func addProgress(atIndexpath indexPath:IndexPath){
        
        guard let managedContext = appDelegate?.persistentContainer.viewContext else {return}
        let currentGoal = goals[indexPath.row]
        
        if currentGoal.goalProgress < currentGoal.goalCompletionValue{
            
            currentGoal.goalProgress = currentGoal.goalProgress + 1
        }else{
            return
        }
        
        do {
            try managedContext.save()
        } catch  {
              debugPrint("could not delet \(error.localizedDescription)")
        }
        
        
    }
    
    
    
    
    
}





