//
//  FinishGoalVC.swift
//  GoalPost
//
//  Created by Asar Sunny on 25/11/2018.
//  Copyright © 2018 Gurudeveloperinc. All rights reserved.
//

import UIKit
import CoreData

class FinishGoalVC: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var creaateGoalBtn: UIButton!
    @IBOutlet weak var pointTextField: UITextField!
    
    
    var goalDescription:String!
    var goalType: goalType!
    
    
    func initData(description:String,type:goalType){
        
        self.goalDescription = description
        self.goalType = type
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        creaateGoalBtn.bindToKeyBoard()

        pointTextField.delegate = self
        
    }

    @IBAction func backBtnPressed(_ sender: Any) {
    dismissDetail()
    }
    
    @IBAction func createGoalBtnPressed(_ sender: UIButton) {
        if pointTextField.text != nil{
            save { (completed) in
                if completed{
                    dismiss(animated: true, completion: nil)
                }
            }
            
        }
        
    }
    
    func save(completion:(_ finished:Bool)->()){
        guard let managedContext = appDelegate?.persistentContainer.viewContext else {return}
        let goal = Goal(context: managedContext)
        
        goal.goalDescription = goalDescription
        goal.goalType = goalType.rawValue
        goal.goalCompletionValue = Int32(pointTextField.text!)!
        goal.goalProgress = Int32(0)
        
        
        do {
            try managedContext.save()
            completion(true)
        } catch  {
            debugPrint("something is wring \(error.localizedDescription)")
            completion(false)
        }
        
        
        
    }
    
    
    
}
