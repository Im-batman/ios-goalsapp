//
//  CreateGoalVC.swift
//  GoalPost
//
//  Created by Asar Sunny on 23/11/2018.
//  Copyright © 2018 Gurudeveloperinc. All rights reserved.
//

import UIKit

class CreateGoalVC: UIViewController,UITextViewDelegate {

    
    
    
    
    @IBOutlet weak var goalTextView: UITextView!
    @IBOutlet weak var shortTermBtn: UIButton!
    @IBOutlet weak var longTermBtn: UIButton!
    @IBOutlet weak var nextBtn: UIButton!
    
    var goalType:goalType = .shortTerm
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nextBtn.bindToKeyBoard()
        goalTextView.delegate = self
//        view.bindToKeyBoard()
    
    }

    
    @IBAction func backBtn(_ sender: Any) {
        
        //custom dismiss 
    dismissDetail()
    }
    
    
    
    @IBAction func shortTermPressed(_ sender: Any) {
        goalType = .shortTerm
        shortTermBtn.setSelectedColor()
        longTermBtn.setDeSelectedColor()
        
    }
    
    @IBAction func longTermPressed(_ sender: Any) {
        goalType = .longTerm
        longTermBtn.setSelectedColor()
        shortTermBtn.setDeSelectedColor()
    }
    
    
    
    @IBAction func nextbtnPressed(_ sender: Any) {
        if goalTextView.text != "" && goalTextView.text != "Whats your goal ?"{
           guard let FinishVC = storyboard?.instantiateViewController(withIdentifier: "FinishGoalVC") as? FinishGoalVC else {return}
            FinishVC.initData(description: goalTextView.text!, type: goalType)
//            presentDetail(FinishVC)
            
            presentingViewController?.presenctSecondaryDetail(FinishVC)
            
        }
        
    }
    
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        goalTextView.text = ""
        goalTextView.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    }
    
    
}
